const TelegramBot = require('node-telegram-bot-api');
require('dotenv').load();
const crearFrase = require('./scripts');

const token = process.env.BOT_TOKEN;
const bot = new TelegramBot(token, {polling: true});

const AMOUNT_INLINE_OPTIONS = process.env.AMOUNT_INLINE_OPTIONS;

const inline_query_options = {
	cache_time: 0,
};

bot.onText(/\/frase/, (msg, match) => {
	const chatId = msg.chat.id;
	bot.sendMessage(chatId, crearFrase());
});

bot.on('inline_query', (event) => {
	bot.answerInlineQuery(event.id,	buildInlineOptions(AMOUNT_INLINE_OPTIONS, event.query), inline_query_options);
});

function buildInlineOptions(amount, palabra) {
	const result = [];
	for(let i = 0; i < amount; i++) {
		const refran = crearFrase(palabra);
		result.push({
			id: i,
			type: 'article',
			title: i == 0 ? 'Refrán Aleatorio' : refran,
			input_message_content: {
				'message_text': refran,
			},
		});
	}
	return result;
}